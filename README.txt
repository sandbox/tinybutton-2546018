-- SUMMARY --

TinyButton is a simple and intuitive interface that makes localizing content a breeze.
Works forever with your existing website � all you need to do is copy and paste the code once!
Please get more information at http://www.tinybutton.com .

For a full description of the module, visit the project page at drupal.org
  http://drupal.org/project/TBD

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual


-- CONFIGURATION --

* Open a TinyButton account at www.tinybutton.com
* Configure your site settings at www.tinybutton.com
* Get the integartion code using the drupal TinyButton module.
* Add the integration code to a body of your header`s block. 
* Access the TinyButton editor by adding '?tinybutton' to your site name: http://YOURSITE/?tinybutton
* Check more details at http://www.tinybutton.com/c/4545034/1/tutorials.html
* IMPORTANT: Please note that localhost is not supported as a Site URL at www.tinybutton.com.
  If you want to test your site locally the TinyButton Editor should be opened manually from the site`s root folder(e.g. http://localhost/YOURSITE/?tinybutton)

-- TROUBLESHOOTING --

* If you are having problem with displaying TinyButton translation module please go to http://www.tinybutton.com or contact our support team at support@tinybutton.com.


-- CONTACT --

Current maintainers:
* Developer at TinyButton (tinybutton) - http://www.drupal.org/user/3276112

