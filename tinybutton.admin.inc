<?php

/**
 * @file
 * TinyButton admin configuration settings page.
 */

/**
 * Tinybutton admin settings form.
 */
function tinybutton_admin_settings() {

  $form = drupal_get_form('tinybutton_add');

  return $form;
}


/**
 * Validate the tinybutton request form.
 */
function tinybutton_add_validate($form, &$form_state) {

  if (strlen($form['login']['#value']) < 5 || strlen($form['password']['#value']) < 5) {
    form_set_error('tinybutton_default_module', t('Please enter your TinyButton credentials.'));
  }
}

/**
 * Create the tinybutton request form.
 */
function tinybutton_add($form, &$form_state) {

  $form['login'] = array(
    '#title' => t('Login'),
    '#name' => 'login',
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => variable_get('tinybutton_user'),
  );

  $form['password'] = array(
    '#title' => t('Password'),
    '#name' => 'password',
    '#type' => 'password',
    '#size' => 50,
    '#maxlength' => 255,
  );

  $form['tb_link'] = array('#markup' => "<p>Don't have an account? <a href='http://www.tinybutton.com'>Signup</a></p>");
  $form['tb_note'] = array('#markup' => "<i><font color='red'>You have to complete your site configuration on <a href='http://www.tinybutton.com'>www.tinybutton.com</a> before proceeding further.</font></i>");

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Get Integration Code'));
  return $form;
}


/**
 * Sumbit the tinybutton integration code request.
 */
function tinybutton_add_submit($form, &$form_state) {

  try {
    $res = request_code($form['login']['#value'], $form['password']['#value']);
    $code = $res[0];
    $success = $res[1];
    if (!$success) {
      variable_set('tinybutton_user', '');
      form_set_error('tinybutton_default_module', $code);
    }
    else {
      variable_set('tinybutton_user', $form['login']['#value']);
      global $base_url;
      $msg = '';
      $msg = '<b>Success!</b><br><br>Here is your integration code: <i>' . htmlspecialchars($code) . '</i>';
      $msg = $msg . '<br><br>Please add this code to a body of your header`s block in order to enable TinyButton on your site.';
      $msg = $msg . '<br><a href="https://www.drupal.org/documentation/modules/block">Click here</a> to get more information on how to use blocks.';
      $msg = $msg . '<br><br> You can access the TinyButton editor by adding \'?tinybutton\' to your site name:';
      $msg = $msg . '<br><a href="' . $base_url . '/?tinybutton">' . $base_url . '/?tinybutton</a>';
      drupal_set_message($msg);
    }
  }
  catch (Exception $e) {
    form_set_error('tinybutton_default_module', 'Unable to get the integration code.<br>' . $e);
  }

  $form_state['redirect'] = 'admin/config/regional';
}

/**
 * Parse the tinybutton service request.
 */
function parse_http_response($string) {

  $headers = array();
  $content = '';
  $str = strtok($string, "\n");
  $h = NULL;
  while ($str !== FALSE) {
    if ($h and trim($str) === '') {
      $h = FALSE;
      continue;
    }

    if ($h !== FALSE and FALSE !== strpos($str, ':')) {
      $h = TRUE;
      list($headername, $headervalue) = explode(':', trim($str), 2);
      $headername = strtolower($headername);
      $headervalue = ltrim($headervalue);

      if (isset($headers[$headername])) {
        $headers[$headername] .= ',' . $headervalue;
      }
      else {
        $headers[$headername] = $headervalue;
      }
    }

    if ($h === FALSE) {
      $content .= $str . "\n";
    }

    $str = strtok("\n");
  }

  return array($headers, trim($content));
}

/**
 * Request the integration code from tinybutton service.
 */
function request_code($email, $password) {

  $code = 'TinyButton: Not Activated';
  $result = TRUE;
  try {
    $timeout = 3;

    $fp = @fsockopen("secure.tinybutton.com", 80, $errno, $errstr, $timeout);
    if (!$fp) {
      return array("TinyButton: Unable to connect to secure.tinybutton.com", FALSE);
    }

    $postdata = "Email=" . $email . "&Password=" . $password;

    $data = "POST /widget/getwidget HTTP/1.1\r\n";
    $data .= "Host: secure.tinybutton.com\r\n";
    $data .= "Content-type: application/x-www-form-urlencoded\r\n";
    $data .= "Content-length: " . strlen($postdata) . "\r\n";
    $data .= "\r\n" . $postdata . "\r\n";

    fwrite($fp, $data);

    stream_set_blocking($fp, TRUE);
    stream_set_timeout($fp, $timeout);
    $info = stream_get_meta_data($fp);

    $response = '';

    while ((!feof($fp)) && (!$info['timed_out'])) {
      $response .= fgets($fp, 4096);
      $info = stream_get_meta_data($fp);
    }

    fclose($fp);

    if (stristr($response, '200 OK') === FALSE) {
      $code = 'TinyButton:Error:No response from secure.tinybutton.com' . $response;
      $result = FALSE;
    }
    else {
      $code = parse_http_response($response)[1];
      if (stristr($code, 'Error') != FALSE) {
        $result = FALSE;
        $code = 'TinyButton:' . $code;
      }
    }
  }
  catch (Exception $e) {
    $code = "TinyButton: Unable to get the integration code.";
    $result = FALSE;
  }

  return array($code, $result);
}
